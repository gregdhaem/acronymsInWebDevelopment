<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180810080904 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE acronyms MODIFY idacronyms INT NOT NULL');
        $this->addSql('DROP INDEX fk_acronyms_users_idx ON acronyms');
        $this->addSql('ALTER TABLE acronyms DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE acronyms DROP users_idusers, CHANGE ilustration illustration VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE acronyms ADD PRIMARY KEY (idacronyms)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE acronyms MODIFY idacronyms INT NOT NULL');
        $this->addSql('ALTER TABLE acronyms DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE acronyms ADD users_idusers INT NOT NULL, CHANGE illustration ilustration VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci');
        $this->addSql('CREATE INDEX fk_acronyms_users_idx ON acronyms (users_idusers)');
        $this->addSql('ALTER TABLE acronyms ADD PRIMARY KEY (idacronyms, users_idusers)');
    }
}
