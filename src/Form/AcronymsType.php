<?php

namespace App\Form;

use App\Entity\Acronyms;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class AcronymsType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('abbreviation', TextType::class, [
                'attr'  => [
                    'name' => 'abbreviation',                           
                    'placeholder' => 'ABC or A.B.C.'
                ],
                'help' => 'The letters of the acronym separated with dots unless prononced as a word i.e. Laser'
            ])

            ->add('keywords', TextType::class, [
                'attr'  => [
                    'name' => 'keywords',                           
                    'placeholder' => 'Alpha Beta Capa...'
                ],
                'help' => 'The keywords (First Letter(s) in Uppercase) acronym.'
            ])

            ->add('definition', TextareaType::class, [
                'attr'  => [
                    'name' => 'definition',                           
                    'placeholder' => 'Name of the fraternity of the web developpers having reached 50... ',
                    'rows' => "10",
                    'cols' => "50"
                ],
                'help' => 'The definition of the acronym.'
            ])

            ->add('illustration', FileType::class, [
                'attr' => [
                    'name' => 'illustration'
                ],
                'help' => 'An optional image of the acronym.'
            ])

            ->add('save', SubmitType::class, [
                'label' => 'Create Acronym Definition Entry',
                'attr'  => [
                    'class' => 'submit-button'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Acronyms::class,
        ]);
    }
}
