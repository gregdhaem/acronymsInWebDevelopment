<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acronyms
 *
 * @ORM\Table(name="acronyms")
 * @ORM\Entity
 */
class Acronyms
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $idacronyms;

    /**
     * @var string
     *
     * @ORM\Column(name="abbreviation", type="string", length=45, nullable=false)
     */
    private $abbreviation;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="string", length=255, nullable=false)
     */
    private $keywords;

    /**
     * @var string
     *
     * @ORM\Column(name="definition", type="text", length=0, nullable=false)
     */
    private $definition;

    /**
     * @var string|null
     *
     * @ORM\Column(name="illustration", type="string", length=255, nullable=true)
     */
    private $illustration;

    /**
     * @var string
     *
     * @ORM\Column(name="creation_date", type="string", length=45, nullable=false)
     */
    private $creationDate;

    public function getIdacronyms(): ?int
    {
        return $this->idacronyms;
    }

    public function getAbbreviation(): ?string
    {
        return $this->abbreviation;
    }

    public function setAbbreviation(string $abbreviation): self
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    public function setKeywords(string $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }

    public function getDefinition(): ?string
    {
        return $this->definition;
    }

    public function setDefinition(string $definition): self
    {
        $this->definition = $definition;

        return $this;
    }

    public function getIllustration(): ?string
    {
        return $this->illustration;
    }

    public function setIllustration(?string $illustration): self
    {
        $this->illustration = $illustration;

        return $this;
    }

    public function getCreationDate(): ?string
    {
        return $this->creationDate;
    }

    public function setCreationDate(string $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }


}
