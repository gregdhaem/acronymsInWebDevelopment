<?php

namespace App\Controller;

use App\Entity\Acronyms;
use App\Form\AcronymsType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class AcronymsController extends Controller
{
    /**
     * @Route("/acronyms", name="acronyms")
     */
    public function acronym(Request $request)
    {

        $acronym = new Acronyms();
        $form = $this->createForm(AcronymsType::class, $acronym);


        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {


            $em = $this->getDoctrine()->getManager();
            $em->persist($acronym);
            $em->flush();

            return $this->redirectToRoute('acronyms');
        }

        return $this->render(
            'acronyms/index.html.twig',
            array('form' => $form->createView())
        );
    }
}

